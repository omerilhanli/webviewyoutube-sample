package com.ilhanli.omer.webviewyoutubesample;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebChromeClient;
import android.webkit.WebView;

/**
 * Created by omerilhanli on 13.12.2017.
 */

/*

<iframe width="560" height="315"
src="https://www.youtube.com/embed/JEJACtfUTyM"
frameborder="0" gesture="media" allow="encrypted-media"
allowfullscreen></iframe>
 */
public class MainActivityEmbed extends AppCompatActivity {

    private WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_embed);

        webView = findViewById(R.id.webView);

        webView.getSettings().setJavaScriptEnabled(true);

        webView.setWebChromeClient(new WebChromeClient() {
        });

        String mimeType = "text/html";

        String encoding = "UTF-8";

        String url = "https://www.youtube.com/embed/JEJACtfUTyM";

        String width = "320";
        String height = "240";

        String html = "<body style=\"margin:0 0 0 0; padding:0 0 0 0;\">" +

                "<iframe width=\"" + width + "\" height=\"" + height + "\" " +

                "class=\"youtube-player\" type=\"" + mimeType + "\" " +

                "src=\"https://www.youtube.com/embed/JEJACtfUTyM\" " +

                "frameborder=\"0\" " +

                "gesture=\"media\" " +

                "allow=\"encrypted-media\"\n" +

                "allowfullscreen>" +

                "</iframe> " +

                "</body>";

        webView.loadDataWithBaseURL("", html, mimeType, encoding, "");

    }

}
